import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/service/product.service';
import { Observable } from 'rxjs';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-product-list',
  templateUrl: 'product-list.component.html',
  styleUrls: ['product-list.component.css']
})
export class ProductListComponent implements OnInit {

  constructor(private productService:ProductService ) { }

  public products: Observable<Product[]> =  new Observable<Product[]>();
  ngOnInit(): void {
    this.products = this.productService.getProductsValues; //TODO: it is better to have state management framework or another layer to keep the state of app models
this.productService.getProducts();
  }

}
