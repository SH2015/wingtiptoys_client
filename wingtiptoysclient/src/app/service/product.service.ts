import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Product } from 'src/app/models/product';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }


  private getProductSubject: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);
 
 
 public getProducts():void {
 
   
  this.http.get<Product[]>( `${environment.apiurl}/api/product/getall/`).subscribe((data)=> {
this.getProductSubject.next(data);
    
  } )

  }
  getProductsValues = this.getProductSubject.asObservable() ; 
}
